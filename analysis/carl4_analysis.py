# %%
import palaestrai
import palaestrai.core
import palaestrai.store
import palaestrai.store.database_util
import palaestrai.store.database_model as paldb

import sqlalchemy as sa

# %%
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

# %matplotlib inline

# %%
import jsonpickle
import jsonpickle.ext.numpy as jsonpickle_numpy
jsonpickle_numpy.register_handlers()

# %%
import io
import os
import pprint
import tempfile
from datetime import datetime
from pathlib import Path

# %%
STEPS = 10000
EID = "20220826-carl4-erikas-reward-v1"
META_DATA = (
    "# Classic ARL\n\n"
    "* 4 Phases\n"
    "* PPO Attacker\n"
    "* DDPG defender\n"
    f"* {STEPS} simulation steps for each phase\n"
    "* training and test\n"
    "* Using the ErikaReward\n"
)
PLOT_PATH = os.path.abspath(os.path.join(os.getcwd(), "results", EID))
os.makedirs(PLOT_PATH, exist_ok=True)
REWARD_VOLTAGE = os.path.join(PLOT_PATH, "reward_voltage.png")
REWARD_LOADING = os.path.join(PLOT_PATH, "reward_loading.png")
REWARD_ERIKA = os.path.join(PLOT_PATH, "reward_erika_reward.png")

with open(os.path.join(PLOT_PATH, "metadata.md"), "w") as f:
    f.write(META_DATA)
    f.write("\n\n*Created at ")
    f.write(datetime.now().strftime("%H:%M:%S%z, %Y-%m-%d"))
    f.write("*\n")

STORE_URI = f"sqlite:///{os.path.abspath(os.path.join(os.getcwd(), 'palaestrai.db'))}"
EXP_RUN_NAME = "Classic ARL four phases with ErikaReward"
ERI_IDX = -1

# %%
rtc = palaestrai.core.RuntimeConfig()

# %%
rtc.__dict__["_store_uri"] = STORE_URI

# %%
rtc

# %%
dbh = palaestrai.store.Session()

# %%
q = sa.select(paldb.ExperimentRun).where(paldb.ExperimentRun.uid == EXP_RUN_NAME)
str(q)


# %%
result = dbh.execute(q).one()
experiment_run_record = result[paldb.ExperimentRun]
experiment_run_record.id, experiment_run_record.uid

# %%
experiment_run_record.experiment_run_instances

# %%
eri_id = experiment_run_record.experiment_run_instances[ERI_IDX].id

# %%
experiment_run_record.experiment_run_instances[ERI_IDX].experiment_run_phases

# %% [markdown]
# 
# ```
# pd.read_sql(
#     sa.select(paldb.Agent)
#     .where(
#         paldb.Agent.experiment_run_phase_id.in_(
#             phase.id for phase in experiment_run_record.experiment_run_instances[ERI_IDX].experiment_run_phases
#         )
#     ),
#     dbh.bind
# )
# ```

# %%
run_phase_ids = [phase.id for phase in experiment_run_record.experiment_run_instances[ERI_IDX].experiment_run_phases]
run_phase_ids

# %% [markdown]
# ```
# defender_record = dbh.execute(
#     sa.select(paldb.Agent)
#     .join(paldb.Agent.experiment_run_phase)
#     .join(paldb.ExperimentRunPhase.experiment_run_instance)
#     .where(paldb.Agent.name=="gandalf" and paldb.ExperimentRunInstance.id == eri_id)
# ).all()
# defender_record = [d[paldb.Agent] for d in defender_record]
# defender_record
# ```

# %% [markdown]
# ```
# attacker_record = dbh.execute(
#     sa.select(paldb.Agent)
#     .join(paldb.Agent.experiment_run_phase)
#     .join(paldb.ExperimentRunPhase.experiment_run_instance)
#     .where(paldb.Agent.name=="sauron" and paldb.ExperimentRunInstance.id == eri_id)
# ).all()
# attacker_record = [a[paldb.Agent] for a in attacker_record]
# attacker_record
# ```

# %% [markdown]
# ```
# test = pd.DataFrame()
# for dr, phase in zip(defender_record, ["phaseII_defender", "phaseIII_att_def", "phaseIV_test_att_def"]):
#     da = pd.read_sql(
#         sa.select(paldb.MuscleAction).where(paldb.MuscleAction.agent_id==dr.id),
#         dbh.bind
#     )
#     da["phase"] = phase
#     test = pd.concat([test, da])
# 
# test
# ```

# %%
rewards = pd.read_sql(
    sa.select(paldb.MuscleAction, paldb.ExperimentRunPhase.uid.label("phase"), paldb.Agent.name)
    .join(paldb.MuscleAction.agent)
    .join(paldb.Agent.experiment_run_phase)
    .where(paldb.ExperimentRunPhase.experiment_run_instance_id == eri_id)
    .order_by(paldb.ExperimentRunPhase.id, paldb.MuscleAction.id),
    dbh.bind
)
rewards

# %%
# rewards["simtimes"][2002]

# %%
# redundant = list(range(1001, 2002)) + list(range(3003, 4004)) + list(range(6006, 8008))
# rewards = rewards.drop(index=redundant)

# %%
# test

# %% [markdown]
# ```
# a_actions = pd.DataFrame()
# for dr, phase in zip(attacker_record, ["phaseI_attacker", "phaseIII_att_def", "phaseIV_test_att_def"]):
#     aa = pd.read_sql(
#         sa.select(paldb.MuscleAction).where(paldb.MuscleAction.agent_id==dr.id),
#         dbh.bind
#     )
#     aa["phase"] = phase
#     a_actions = pd.concat([a_actions, aa])
# 
# a_actions
# ```

# %%


# %%
def unpack_reward(x, i):
    if not x:
        print("Cell is empty!")
        return 0
    try:
        val = x[i]["reward_value"]["value"]
    except KeyError as err:
        try:
            val = x[i]["reward_value"]["values"][0]
        except KeyError as err:
            print(f"Error at index {i}: {err}")
            print(x[i])
            raise err
    except TypeError as err:
        return int(x[i]["reward_value"])
    except IndexError as err:
        print(i)
        print(x)
        print(x[i])
        print(x[i]["reward_value"])
        
    return float(val) if x else 0.0

cols = ["vm_pu_min", "vm_pu_max", "vm_pu_median", "vm_pu_mean", "vm_pu_std", "line_min", "line_max", "line_median", "line_mean", "line_std", "ErikaReward"]

for idx, name in enumerate(cols):
    print(idx, name)
    rewards[name] = rewards.rewards.apply(lambda x: unpack_reward(x, idx))
    # a_actions[name] = a_actions.rewards.apply(lambda x: unpack_reward(x, idx))

rewards




# %%
# rewards[rewards.phase == "phaseI_attacker"]

# %%
unique_rewards = rewards.drop(
    rewards.loc[(rewards.phase.str.contains("phaseIII_att_def") | rewards.phase.str.contains("phaseIV_test_att_def")) & rewards.name.str.contains("sauron")].index
)
unique_rewards
old_rewards = rewards
rewards = unique_rewards

# %%
rm = 2

# phase1 = a_actions[a_actions["phase"] == "phaseI_attacker"]
# phase2 = d_actions[d_actions["phase"] == "phaseII_defender"]
# phase3 = d_actions[d_actions["phase"] == "phaseIII_att_def"]
# phase4 = d_actions[d_actions["phase"] == "phaseIV_test_att_def"]

# rewards = pd.concat([phase1, phase2, phase3, phase4])
# rewards = rewards.reset_index(drop=True)

rewards["vm_pu_min_mean"] = rewards["vm_pu_min"].rolling(rm).mean()
rewards["vm_pu_max_mean"] = rewards["vm_pu_max"].rolling(rm).mean()
rewards["vm_pu_mean_mean"] = rewards["vm_pu_mean"].rolling(rm).mean()
rewards["vm_pu_median_mean"] = rewards["vm_pu_median"].rolling(rm).mean()
rewards["line_min_mean"] = rewards["line_min"].rolling(rm).mean()
rewards["line_max_mean"] = rewards["line_max"].rolling(rm).mean()
rewards["line_median_mean"] = rewards["line_median"].rolling(rm).mean()
rewards["line_mean_mean"] = rewards["line_mean"].rolling(rm).mean()
rewards

# %%
index = np.arange(len(rewards.index))
plt.figure(figsize=(20, 6))
plt.plot(index, rewards.vm_pu_min_mean, color="green", label="MIN")
plt.plot(index, rewards.vm_pu_max_mean, color="red", label="MAX")
plt.plot(index, rewards.vm_pu_median_mean, color="purple", label="MEDIAN")
plt.plot(index, rewards.vm_pu_mean_mean, color="blue", label="MEAN")
plt.vlines(STEPS - 0.5, ymin=0.75, ymax=1.1, label="phaseI_attacker", linestyles="dashed")
plt.vlines(STEPS*2 - 0.5, ymin=0.75, ymax=1.1, label="phaseII_defender", linestyles="dashed")
plt.vlines(STEPS*3 - 0.5, ymin=0.75, ymax=1.1, label="phaseIII_att_def", linestyles="dashed")
plt.vlines(STEPS*4 - 0.5, ymin=0.75, ymax=1.1, label="phaseIV_test_att_def", linestyles="dashed")
plt.xlabel("Step Index")
plt.ylabel("Voltage Magnitude (p.u.)")
plt.title("Reward Voltage")
plt.legend()

plt.savefig(REWARD_VOLTAGE, dpi=300, bbox_inches="tight")


# %%
plt.figure(figsize=(20, 6))
plt.plot(index, rewards.line_min_mean, color="green", label="MIN")
plt.plot(index, rewards.line_max_mean, color="red", label="MAX")
plt.plot(index, rewards.line_median_mean, color="purple", label="MEDIAN")
plt.plot(index, rewards.line_mean_mean, color="blue", label="MEAN")
plt.vlines(STEPS - 0.5, ymin=0.0, ymax=100, label="phaseI_attacker", linestyles="dashed")
plt.vlines(STEPS*2 - 0.5, ymin=0.0, ymax=100, label="phaseII_defender", linestyles="dashed")
plt.vlines(STEPS*3 - 0.5, ymin=0.0, ymax=100, label="phaseIII_att_def", linestyles="dashed")
plt.vlines(STEPS*4 - 0.5, ymin=0.0, ymax=100, label="phaseIV_test_att_def", linestyles="dashed")
plt.xlabel("Step Index")
plt.ylabel("Loading [%]")
plt.title("Reward Line Loading")
plt.legend()

plt.savefig(REWARD_LOADING, dpi=300, bbox_inches="tight")

# %%
plt.figure(figsize=(20, 6))

plt.plot(index, rewards.ErikaReward, color="purple", label="ErikaReward")
plt.vlines(STEPS - 0.5, ymin=-300, ymax=0, label="phaseI_attacker", linestyles="dashed")
plt.vlines(STEPS*2 - 0.5, ymin=-300, ymax=0, label="phaseII_defender", linestyles="dashed")
plt.vlines(STEPS*3 - 0.5, ymin=-300, ymax=0, label="phaseIII_att_def", linestyles="dashed")
plt.vlines(STEPS*4 - 0.5, ymin=-300, ymax=0, label="phaseIV_test_att_def", linestyles="dashed")
plt.xlabel("Step Index")
plt.ylabel("Points")
plt.title("ErikaReward")
plt.legend()
plt.savefig(REWARD_ERIKA, dpi=300, bbox_inches="tight")

# %%



